En el presente proyecto se especifican 3 codigos ya que se ha combinado los 
lenguajes de Python, MicroPython y Arduino. 


Para el control de la base movil se usó una palanca de PS4, con la cual se
envia los datos al ESP01 via WiFi con la ayuda de un código de Python. Para el
código de Python se uso como librería Pygame para la obtención de los datos.
Esta libreria de Python no cumple la función de driver para la palanca/mando,
sin embargo windows 10 es compatible con la palanca de PS4, en caso de que se
use una de otra marca o genérica, es posible que se necesite un driver
para que la PC la reconozca antes de usarla en el código. Usamos la creación de
un objeto socket para mediante el hacer la conexión con el ESP01 al estar en la
misma red WiFi.


MicroPython fue usado para establecer la conexión, por lo que este codigo nos
permite recibir a través de un objeto socket los datos enviados por Python para
luego enviarlos al Arduino y que este pueda controlar los motores de acuerdo a
los datos.
